package tdd.vendingMachine.state;

import org.junit.Before;
import org.junit.Test;

import tdd.vendingMachine.entities.Coin;
import tdd.vendingMachine.exception.UnableToRemoveCoinFromWalletException;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DefaultCoinWalletTest {

  private DefaultCoinWallet defaultCoinWallet;

  @Before
  public void setUp() throws Exception {
    defaultCoinWallet = new DefaultCoinWallet(false);
  }


  @Test
  public void testAddCoinWhenCoinNotYetInTheWallet() throws Exception {
    assertEquals(0, defaultCoinWallet.getCoinsMap().keySet().size());

    Coin sampleCoin = new Coin(new BigDecimal(0.50));

    defaultCoinWallet.addCoin(sampleCoin);
    assertEquals(1, defaultCoinWallet.getCoinsMap().keySet().size());
    assertEquals((Integer) 1, defaultCoinWallet.getCoinsMap().get(sampleCoin));
  }

  @Test
  public void testAddCoinWhenCoinAlreadyInTheWallet() throws Exception {
    Coin sampleCoin = new Coin(new BigDecimal(0.50));
    defaultCoinWallet.addCoin(sampleCoin);

    defaultCoinWallet.addCoin(sampleCoin);
    assertEquals(1, defaultCoinWallet.getCoinsMap().keySet().size());
    assertEquals((Integer) 2, defaultCoinWallet.getCoinsMap().get(sampleCoin));
  }

  @Test
  public void testRemoveCoinWhenCoinAlreadyInTheWallet() throws Exception {
    Coin sampleCoin = new Coin(new BigDecimal(0.50));
    defaultCoinWallet.addCoin(sampleCoin);

    defaultCoinWallet.removeCoin(sampleCoin);
    assertEquals(1, defaultCoinWallet.getCoinsMap().keySet().size());
    assertEquals((Integer) 0, defaultCoinWallet.getCoinsMap().get(sampleCoin));
  }

  @Test(expected = UnableToRemoveCoinFromWalletException.class)
  public void testRemoveCoinWhenCoinNotInTheWallet() throws Exception {
    Coin sampleCoin = new Coin(new BigDecimal(0.50));
    defaultCoinWallet.removeCoin(sampleCoin);
  }

  @Test(expected = UnableToRemoveCoinFromWalletException.class)
  public void testRemoveCoinWhenNUmberOfCoinsInWalletIsZero() throws Exception {
    Coin sampleCoin = new Coin(new BigDecimal(0.50));
    defaultCoinWallet.addCoin(sampleCoin);
    defaultCoinWallet.removeCoin(sampleCoin);
    defaultCoinWallet.removeCoin(sampleCoin);
  }
}
