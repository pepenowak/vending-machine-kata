package tdd.vendingMachine.state;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import tdd.vendingMachine.change.ChangeCalculator;
import tdd.vendingMachine.change.OperationResultWrapper;
import tdd.vendingMachine.entities.Coin;
import tdd.vendingMachine.entities.Product;
import tdd.vendingMachine.entities.Shelf;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultVendingMachineStateTest {
  private DefaultVendingMachineState vendingMachineState;

  @Before
  public void setUp() throws Exception {
    vendingMachineState = new DefaultVendingMachineState();
  }

  @Test
  public void testInsertChangeWhenNoProductSelected() throws Exception {
    assertNull(vendingMachineState.insertChange(new BigDecimal(0.5)));
  }

  @Test
  public void testInsertChangeWhenInsertedAmountIsNotBiggerThanPrice() throws Exception {
    vendingMachineState.setSelectedShelf(new Shelf(new Product("Sample", new BigDecimal(2.0)), 2));
    OperationResultWrapper result = vendingMachineState.insertChange(new BigDecimal(1.0));
    assertEquals(0, new BigDecimal(1.0).compareTo(vendingMachineState.getInsertedAmount()));
    assertEquals((Integer) 1, vendingMachineState.getInsertedCoinsWallet().getCoinsMap().
        get(new Coin(new BigDecimal(1))));
    assertNull(result);
  }

  @Test
  public void testInsertChangeWhenInsertedAmountIsBiggerThanPrice() throws Exception {
    ChangeCalculator changeCalculator = mock(ChangeCalculator.class);
    Map<Coin, Integer> resultMap = new HashMap<>();
    resultMap.put(new Coin(new BigDecimal(1.00)), 3);
    when(changeCalculator.calculateChange(any(), any())).thenReturn(resultMap);
    vendingMachineState.setSimpleChangeCalculator(changeCalculator);

    vendingMachineState.setSelectedShelf(new Shelf(new Product("Sample", new BigDecimal(2.0)), 2));
    OperationResultWrapper result = vendingMachineState.insertChange(new BigDecimal(5.0));
    assertEquals(0, new BigDecimal(0).compareTo(vendingMachineState.getInsertedAmount()));
    assertEquals(new HashMap<Coin, Integer>(), vendingMachineState.getInsertedCoinsWallet().getCoinsMap());
    assertTrue(result.isAbleToGiveChange());

    assertEquals((Integer) 3, result.getChange().get(new Coin(new BigDecimal(1.00))));
  }

  @Test
  public void testCalculateChangeWhenNotPossibleToGiveChange() throws Exception {
    ChangeCalculator simpleChangeCalculator = mock(ChangeCalculator.class);
    ChangeCalculator ilpChangeCalculator = mock(ChangeCalculator.class);
    when(simpleChangeCalculator.calculateChange(any(), any())).thenReturn(null);
    when(ilpChangeCalculator.calculateChange(any(), any())).thenReturn(null);
    CoinWallet insertedCoinsWallet = mock(CoinWallet.class);

    vendingMachineState.setSimpleChangeCalculator(simpleChangeCalculator);
    vendingMachineState.setIlpChangeCalculator(ilpChangeCalculator);
    vendingMachineState.setInsertedCoinsWallet(insertedCoinsWallet);

    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(5.0)), 2);
    vendingMachineState.setSelectedShelf(selectedShelf);

    OperationResultWrapper result = vendingMachineState.
        calculateChangeAndReleaseProduct(new BigDecimal(1.0));
    verify(insertedCoinsWallet, times(1)).setCoinsMap(new HashMap<>());
    assertFalse(result.isAbleToGiveChange());
    assertNull(result.getChange());
  }

  @Test
  public void testCalculateChangeWhenNoProductSelected() throws Exception {
    OperationResultWrapper result = vendingMachineState.
        calculateChangeAndReleaseProduct(new BigDecimal(1.0));
    assertNull(result);
  }

  @Test
  public void testCalculateChangeWhenPossibleToGiveChange() throws Exception {
    ChangeCalculator simpleChangeCalculator = mock(ChangeCalculator.class);
    ChangeCalculator ilpChangeCalculator = mock(ChangeCalculator.class);
    when(simpleChangeCalculator.calculateChange(any(), any())).thenReturn(null);
    when(ilpChangeCalculator.calculateChange(any(), any())).thenReturn(null);
    Map<Coin, Integer> changeMap = new HashMap<>();
    changeMap.put(new Coin(new BigDecimal(1.00)), 3);
    when(ilpChangeCalculator.calculateChange(any(), any())).thenReturn(changeMap);

    CoinWallet machineCoinWallet = mock(CoinWallet.class);
    CoinWallet insertedCoinsWallet = mock(CoinWallet.class);
    Map<Coin, Integer> insertedCoinsMap = new HashMap<>();
    insertedCoinsMap.put(new Coin(new BigDecimal(2.0)), 3);
    when(insertedCoinsWallet.getCoinsMap()).thenReturn(insertedCoinsMap);

    vendingMachineState.setSimpleChangeCalculator(simpleChangeCalculator);
    vendingMachineState.setIlpChangeCalculator(ilpChangeCalculator);
    vendingMachineState.setVendingMachineCoinWallet(machineCoinWallet);
    vendingMachineState.setInsertedCoinsWallet(insertedCoinsWallet);
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(5.0)), 2);
    vendingMachineState.setSelectedShelf(selectedShelf);

    OperationResultWrapper result = vendingMachineState.
        calculateChangeAndReleaseProduct(new BigDecimal(1.0));
    verify(machineCoinWallet, times(1)).addCoins(new Coin(new BigDecimal(2.0)), 3);
    verify(machineCoinWallet, times(1)).removeCoins(new Coin(new BigDecimal(1.00)), 3);
    verify(insertedCoinsWallet, times(1)).setCoinsMap(new HashMap<>());
    assertEquals(new BigDecimal(0), vendingMachineState.getInsertedAmount());
    assertEquals(changeMap, result.getChange());
    assertTrue(result.isAbleToGiveChange());
  }

  @Test
  public void testSelectShelfWhenQuantityIsBiggerThanZero() throws Exception {
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(5.0)), 2);
    vendingMachineState.setSelectedShelf(selectedShelf);
    assertEquals(selectedShelf, vendingMachineState.getSelectedShelf());
  }

  @Test
  public void testSelectShelfWhenQuantityIsZero() throws Exception {
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(5.0)), 0);
    vendingMachineState.setSelectedShelf(selectedShelf);
    assertNull(vendingMachineState.getSelectedShelf());
  }

  @Test
  public void testCancelOperation() throws Exception {
    CoinWallet insertedCoinsWallet = mock(CoinWallet.class);
    vendingMachineState.setInsertedCoinsWallet(insertedCoinsWallet);
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(5.0)), 0);
    vendingMachineState.setSelectedShelf(selectedShelf);
    vendingMachineState.cancelOperation();
    assertNull(vendingMachineState.getSelectedShelf());
    verify(insertedCoinsWallet, times(1)).setCoinsMap(new HashMap<>());
  }
}