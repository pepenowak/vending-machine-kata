package tdd.vendingMachine.change;

import junit.framework.TestCase;

import org.junit.Test;

import tdd.vendingMachine.state.CoinWallet;
import tdd.vendingMachine.entities.Coin;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SimpleChangeCalculatorTest extends TestCase {

  private SimpleChangeCalculator simpleChangeCalculator;

  public void setUp() throws Exception {
    simpleChangeCalculator = new SimpleChangeCalculator();
  }

  public void testReturnsCorrectChangeWhenCoinsAvailable() throws Exception {
    Coin coin20p = new Coin(new BigDecimal(0.2));
    Coin coin50p = new Coin(new BigDecimal(0.5));
    Coin coin1 = new Coin(new BigDecimal((1)));
    Coin coin2 = new Coin(new BigDecimal(2));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin2, 3);
    availableCoins.put(coin1, 2);
    availableCoins.put(coin50p, 0);
    availableCoins.put(coin20p, 3);

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);

    Map<Coin, Integer> result = simpleChangeCalculator.calculateChange(
        new BigDecimal(7.40).setScale(2, BigDecimal.ROUND_HALF_UP), coinWallet);
    assertEquals((Integer) 3, result.get(coin2));
    assertEquals((Integer) 1, result.get(coin1));
    assertEquals((Integer) 0, result.get(coin50p));
    assertEquals((Integer) 2, result.get(coin20p));
  }

  @Test
  public void testReturnsNullWhenThereAreNotEnoughCoins() throws Exception {
    Coin coin20p = new Coin(new BigDecimal(0.2));
    Coin coin50p = new Coin(new BigDecimal(0.5));
    Coin coin1 = new Coin(new BigDecimal((1)));
    Coin coin2 = new Coin(new BigDecimal(2));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin2, 3);
    availableCoins.put(coin1, 2);
    availableCoins.put(coin50p, 0);
    availableCoins.put(coin20p, 1);

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);

    Map<Coin, Integer> result = simpleChangeCalculator.calculateChange(
        new BigDecimal(7.40).setScale(2, BigDecimal.ROUND_HALF_UP), coinWallet);
    assertNull(result);
  }

  /**
   * SimpleChangeCalculator has a disadvantage of always selecting the biggest possible coin if it
   * is available. For example, when giving change of 8.00, having coins 1 x 5.00, 4 x 2.00, it will
   * not work correctly. It will initially select 5.00 coin and have 3.00 left, because there are
   * only 2.00 coins left, it cannot give correct change. However, it could simply give 4 x 2.00.
   */
  public void testNotGivingCorrectChangeDespiteOtherCoinsAvailable() throws Exception {
    Coin coin5 = new Coin(new BigDecimal(5));
    Coin coin2 = new Coin(new BigDecimal(2));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin5, 1);
    availableCoins.put(coin2, 4);

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);

    Map<Coin, Integer> result = simpleChangeCalculator.calculateChange(
        new BigDecimal(7.40).setScale(2, BigDecimal.ROUND_HALF_UP), coinWallet);
    assertNull(result);
  }
}
