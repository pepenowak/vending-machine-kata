package tdd.vendingMachine.change;

import org.junit.Before;
import org.junit.Test;

import tdd.vendingMachine.state.CoinWallet;
import tdd.vendingMachine.entities.Coin;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ILPChangeCalculatorTest {
  private ILPChangeCalculator ilpChangeCalculator;

  @Before
  public void setUp() throws Exception {
    ilpChangeCalculator = new ILPChangeCalculator();
  }

  @Test
  public void testReturnsChangeWhenResultIsAvailable() throws Exception {
    Coin coin5 = new Coin(new BigDecimal(5));
    Coin coin2 = new Coin(new BigDecimal(2));
    Coin coin20p = new Coin(new BigDecimal(0.2));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin5, 1);
    availableCoins.put(coin2, 4);
    availableCoins.put(coin20p, 0);

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);
    Map<Coin, Integer> coinIntegerMap = ilpChangeCalculator.calculateChange(
        new BigDecimal(8), coinWallet);
    assertNotNull(coinIntegerMap);
    assertEquals((Integer) 0, coinIntegerMap.get(coin5));
    assertEquals((Integer) 4, coinIntegerMap.get(coin2));
    assertEquals((Integer) 0, coinIntegerMap.get(coin20p));
  }

  @Test
  public void testReturnsNullWhenObjectiveFunctionValueIsLowerThanRequiredChange()
      throws Exception {
    Coin coin5 = new Coin(new BigDecimal(5));
    Coin coin2 = new Coin(new BigDecimal(2));
    Coin coin20p = new Coin(new BigDecimal(0.2));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin5, 1);
    availableCoins.put(coin2, 3);
    availableCoins.put(coin20p, 0);

    // Maximum possible objective function value is 7.00 (1 x 5.00 + 1 x 2.00)
    // Therefore method should return NULL as 7.00 != 8.00 (required change)

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);
    Map<Coin, Integer> coinIntegerMap = ilpChangeCalculator.calculateChange(
        new BigDecimal(8), coinWallet);
    assertNull(coinIntegerMap);
  }

  @Test
  public void testReturnsNullWhenThereIsNoSolutionToILP() throws Exception {
    Coin coin5 = new Coin(new BigDecimal(5));
    Map<Coin, Integer> availableCoins = new HashMap<>();
    availableCoins.put(coin5, 1);

    // Maximum possible objective function value is 7.00 (1 x 5.00 + 1 x 2.00)
    // Therefore method should return NULL as 7.00 != 8.00 (required change)

    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(availableCoins);
    Map<Coin, Integer> coinIntegerMap = ilpChangeCalculator.calculateChange(
        new BigDecimal(2), coinWallet);
    assertNull(coinIntegerMap);
  }

  @Test
  public void testCalculateChangeWhenCoinWallwtEmpty() throws Exception {
    CoinWallet coinWallet = mock(CoinWallet.class);
    when(coinWallet.getCoinsMap()).thenReturn(new HashMap<>());
    assertNull(ilpChangeCalculator.calculateChange(new BigDecimal(2), coinWallet));
  }
}