package tdd.vendingMachine.display;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import tdd.vendingMachine.change.OperationResultWrapper;
import tdd.vendingMachine.entities.Product;
import tdd.vendingMachine.entities.Shelf;
import tdd.vendingMachine.state.VendingMachineState;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConsoleDisplayTest {

  private ConsoleDisplay consoleDisplay;
  private VendingMachineState vendingMachineState;

  @Before
  public void setUp() throws Exception {
    vendingMachineState = mock(VendingMachineState.class);
    consoleDisplay = new ConsoleDisplay(vendingMachineState);
  }

  @Test
  public void testGetNumberInputWhenWrongNumberFormat() throws Exception {
    int result = consoleDisplay.checkNumberInput("wrongFormat");
    assertEquals(-1, result);
  }

  @Test
  public void testGetNumberInputWhenCorrectFormat() throws Exception {
    int result = consoleDisplay.checkNumberInput("4");
    assertEquals(4, result);
  }

  @Test
  public void testVerifyInsertChangeCalled() throws Exception {
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(1.0)), 2);
    when(vendingMachineState.getSelectedShelf()).thenReturn(selectedShelf);
    consoleDisplay.insertMoney(5.00);
    verify(vendingMachineState, times(1)).insertChange(new BigDecimal(5.00).setScale(2,
        BigDecimal.ROUND_HALF_UP));
  }


  @Test
  public void testVerifyResultGetChangeWhenAbleToGiveChange() throws Exception {
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(1.0)), 2);
    when(vendingMachineState.getSelectedShelf()).thenReturn(selectedShelf);
    OperationResultWrapper resultWrapper = mock(OperationResultWrapper.class);
    when(resultWrapper.isAbleToGiveChange()).thenReturn(true);
    when(vendingMachineState.insertChange(
        new BigDecimal(5.00).setScale(2, BigDecimal.ROUND_HALF_UP)))
        .thenReturn(resultWrapper);

    consoleDisplay.insertMoney(5.00);
    verify(resultWrapper, times(1)).getChange();
  }

  @Test
  public void testVerifyNoGetChangeWhenNotAbleToGiveChange() throws Exception {
    Shelf selectedShelf = new Shelf(new Product("Sample", new BigDecimal(1.0)), 2);
    when(vendingMachineState.getSelectedShelf()).thenReturn(selectedShelf);
    OperationResultWrapper resultWrapper = mock(OperationResultWrapper.class);
    when(resultWrapper.isAbleToGiveChange()).thenReturn(false);
    when(vendingMachineState.insertChange(
        new BigDecimal(5.00).setScale(2, BigDecimal.ROUND_HALF_UP)))
        .thenReturn(resultWrapper);

    consoleDisplay.insertMoney(5.00);
    verify(resultWrapper, times(0)).getChange();
  }
}