package tdd.vendingMachine.display;

import tdd.vendingMachine.entities.Shelf;

import java.util.List;

public interface Display {

  boolean printOptions(List<Shelf> shelves);
}
