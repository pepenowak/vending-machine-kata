package tdd.vendingMachine.display;

import tdd.vendingMachine.change.OperationResultWrapper;
import tdd.vendingMachine.entities.Shelf;
import tdd.vendingMachine.state.VendingMachineState;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class ConsoleDisplay implements Display {

  private static final String INSERTED_COIN_5 = "coin5";
  private static final String INSERTED_COIN_2 = "coin2";
  private static final String INSERTED_COIN_1 = "coin1";
  private static final String INSERTED_COIN_050 = "coin0.5";
  private static final String INSERTED_COIN_020 = "coin0.2";
  private static final String INSERTED_COIN_010 = "coin0.1";
  private static final String CANCEL = "cancel";
  private static final String EXIT = "exit";
  private Scanner reader;
  private VendingMachineState vendingMachineState;
  private boolean pressedExit;

  public ConsoleDisplay(VendingMachineState vendingMachineState) {
    this.reader = new Scanner(System.in);
    this.vendingMachineState = vendingMachineState;
    this.pressedExit = false;
  }

  public boolean printOptions(List<Shelf> shelves) {
    System.out.println();
    System.out.println("YOUR CURRENT SELECTION:");
    System.out.println(vendingMachineState.getSelectedShelf() != null ?
        vendingMachineState.getSelectedShelf() :
        "You have not selected any product yet.");
    if (vendingMachineState.getSelectedShelf() != null && vendingMachineState.getInsertedAmount()
        .compareTo(new BigDecimal(0)) > 0) {
      System.out.println("Already inserted: " + vendingMachineState.getInsertedAmount());
      System.out.println("Remaining amount: " + vendingMachineState.getRemainingAmount());
    }
    System.out.println();
    System.out.println("Please select one of the following options:");
    System.out.println("(1) Type 0-" + (shelves.size() - 1) + " in order to get information " +
        "about the product on shelf of that number.");
    System.out.println("(2) After selecting a product, if you want to buy it, start inserting " +
        "money by typing coin5, coin2, coin1, coin0.5, coin0.2 and coin0.1");
    System.out.println("(3) Type cancel to get all your money back.");
    System.out.println("(4) Type exit to stop the vending machine.");
    String input = reader.nextLine().trim();
    switch (input) {
      case INSERTED_COIN_5:
        insertMoney(5);
        break;
      case INSERTED_COIN_2:
        insertMoney(2);
        break;
      case INSERTED_COIN_1:
        insertMoney(1);
        break;
      case INSERTED_COIN_050:
        insertMoney(0.5);
        break;
      case INSERTED_COIN_020:
        insertMoney(0.2);
        break;
      case INSERTED_COIN_010:
        insertMoney(0.1);
        break;
      case CANCEL:
        vendingMachineState.cancelOperation();
        System.out.println("Pressed CANCEL. Returning all inserted money. Please start again.");
        break;
      case EXIT:
        pressedExit = true;
        break;
      default:
        int num = checkNumberInput(input);
        if (num >= 0 && num <= shelves.size() - 1) {
          vendingMachineState.setSelectedShelf(shelves.get(num));
        } else {
          System.out.println("WRONG selection. Please try again.");
        }
    }
    return pressedExit;
  }

  void insertMoney(double value) {
    if (vendingMachineState.getSelectedShelf() != null) {
      OperationResultWrapper resultWrapper = vendingMachineState.insertChange(new BigDecimal(value)
          .setScale(2, BigDecimal.ROUND_HALF_UP));
      if (resultWrapper != null && resultWrapper.isAbleToGiveChange()) {
        System.out.println("Thanks for buying your product! Remember to collect your change!");
        System.out.println("Change: " + resultWrapper.getChange());
      } else if (resultWrapper != null) {
        System.out.println("Not able to give change. Returning all inserted money.");
        System.out.println("Please start again.");
      }
    } else {
      System.out.println("You must select a product before inserting money.");
    }
  }

  int checkNumberInput(String input) {
    try {
      return Integer.parseInt(input);
    } catch (NumberFormatException e) {
      return -1;
    }
  }
}
