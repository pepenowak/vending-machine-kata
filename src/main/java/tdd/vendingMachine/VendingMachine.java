package tdd.vendingMachine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import tdd.vendingMachine.display.ConsoleDisplay;
import tdd.vendingMachine.display.Display;
import tdd.vendingMachine.entities.Product;
import tdd.vendingMachine.entities.Shelf;
import tdd.vendingMachine.state.DefaultVendingMachineState;
import tdd.vendingMachine.state.VendingMachineState;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class VendingMachine {

  private static final Logger LOGGER = LogManager.getLogger(VendingMachine.class);
  private static final String SEPARATOR = ",";
  private boolean shouldStop;

  private Display display;
  private VendingMachineState vendingMachineState;

  public VendingMachine() {
    this.vendingMachineState = new DefaultVendingMachineState();
    this.display = new ConsoleDisplay(vendingMachineState);
    this.shouldStop = false;
    init();
  }

  private void init() {
    ClassLoader classLoader = getClass().getClassLoader();
    InputStream in = classLoader.getResourceAsStream("products.csv");
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        String[] array = line.split(SEPARATOR);
        if (array.length != 3) {
          LOGGER.warn("Ignoring product initialization. Wrong size of product information array. " +
              "Could not parse the following line: " + line);
          continue;
        }
        try {
          String name = array[0];
          double price = Double.parseDouble(array[1]);
          int quantity = Integer.parseInt(array[2]);
          Product product = new Product(name, new BigDecimal(price));
          vendingMachineState.addShelf(new Shelf(product, quantity));
        } catch (NumberFormatException e) {
          LOGGER.warn("Ignoring product initialization. Wrong product information. Could not " +
              "parse the following line: " + line);
        }

      }
      reader.close();
    } catch (IOException e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  void start() {
    while (!shouldStop) {
      tick();
    }
  }

  private void tick() {
    shouldStop = display.printOptions(vendingMachineState.getShelves());
  }
}
