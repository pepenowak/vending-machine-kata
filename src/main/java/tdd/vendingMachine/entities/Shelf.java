package tdd.vendingMachine.entities;

/**
 * Represents a shelf in the vending machine. It stores information about product type and quantity.
 */
public class Shelf {

  private Product product;
  private int quantity;

  public Shelf(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public void removeSingleProduct() {
    this.quantity--;
  }

  public Product getProduct() {
    return product;
  }

  public int getQuantity() {
    return quantity;
  }

  @Override
  public String toString() {
    return "Shelf{" +
        "product=" + product +
        ", quantity=" + quantity +
        '}';
  }
}
