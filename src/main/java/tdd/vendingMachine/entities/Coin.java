package tdd.vendingMachine.entities;

import java.math.BigDecimal;

/**
 * Represents coins used to pay/give change in the vending machine.
 */
public class Coin {

  private BigDecimal value;

  public Coin(BigDecimal value) {
    this.value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public BigDecimal getValue() {
    return this.value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Coin coin = (Coin) o;

    return value != null ? value.equals(coin.value) : coin.value == null;

  }

  @Override
  public int hashCode() {
    return value != null ? value.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Coin{" +
        "value=" + value +
        '}';
  }
}
