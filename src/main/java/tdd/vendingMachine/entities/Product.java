package tdd.vendingMachine.entities;

import java.math.BigDecimal;

/**
 * Product stores basic information about the product including name and price.
 */
public class Product {

  private String name;
  private BigDecimal price;

  public Product(String name, BigDecimal price) {
    this.name = name;
    this.price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  public BigDecimal getPrice() {
    return price;
  }

  @Override
  public String toString() {
    return "Product{" +
        "name='" + name + '\'' +
        ", price=" + price + '}';
  }
}
