package tdd.vendingMachine.state;

import java.math.BigDecimal;
import java.util.List;

import tdd.vendingMachine.change.OperationResultWrapper;
import tdd.vendingMachine.entities.Shelf;

public interface VendingMachineState {
  OperationResultWrapper insertChange(BigDecimal amount);

  OperationResultWrapper calculateChangeAndReleaseProduct(BigDecimal changeRequired);

  void setSelectedShelf(Shelf selectedShelf);

  void addShelf(Shelf shelf);

  List<Shelf> getShelves();

  Shelf getSelectedShelf();

  BigDecimal getRemainingAmount();

  void cancelOperation();

  BigDecimal getInsertedAmount();

  CoinWallet getInsertedCoinsWallet();
}
