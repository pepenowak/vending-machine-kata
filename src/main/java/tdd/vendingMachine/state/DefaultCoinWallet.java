package tdd.vendingMachine.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import tdd.vendingMachine.entities.Coin;
import tdd.vendingMachine.exception.UnableToRemoveCoinFromWalletException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * DefaultCoinWallet stores the map of all available coins and provides methods to add/remove coins.
 */
public class DefaultCoinWallet implements CoinWallet {

  private static final Logger logger = LogManager.getLogger(DefaultCoinWallet.class);

  private Map<Coin, Integer> availableCoins;

  public DefaultCoinWallet(boolean shouldAddInitialMoney) {
    this.availableCoins = new HashMap<>();
    if (shouldAddInitialMoney) {
      for (int i = 0; i < 10; i++) {
        addCoin(new Coin(new BigDecimal(5)));
        addCoin(new Coin(new BigDecimal(2)));
        addCoin(new Coin(new BigDecimal(1)));
        addCoin(new Coin(new BigDecimal(0.5)));
        addCoin(new Coin(new BigDecimal(0.2)));
        addCoin(new Coin(new BigDecimal(0.1)));
      }
    }
  }

  public void addCoin(Coin coin) {
    if (availableCoins.containsKey(coin)) {
      availableCoins.put(coin, availableCoins.get(coin) + 1);
    } else {
      availableCoins.put(coin, 1);
    }
  }

  public void removeCoin(Coin coin) {
    if (!availableCoins.containsKey(coin) || availableCoins.get(coin) < 1) {
      throw new UnableToRemoveCoinFromWalletException("There is no coin that could be removed. " +
          "Please verify why application tried to remove coin that is not available");
    }
    availableCoins.put(coin, availableCoins.get(coin) - 1);
  }

  @Override
  public void addCoins(Coin coin, int number) {
    for (int i = 0; i < number; i++) {
      addCoin(coin);
    }
  }

  @Override
  public void removeCoins(Coin coin, int number) {
    for (int i = 0; i < number; i++) {
      removeCoin(coin);
    }
  }

  @Override
  public Map<Coin, Integer> getCoinsMap() {
    return availableCoins;
  }


  public void setCoinsMap(Map<Coin, Integer> wallet) {
    this.availableCoins = wallet;
  }
}
