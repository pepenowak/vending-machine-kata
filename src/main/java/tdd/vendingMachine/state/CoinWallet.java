package tdd.vendingMachine.state;

import tdd.vendingMachine.entities.Coin;

import java.util.Map;

public interface CoinWallet {
  void addCoins(Coin coin, int number);

  void removeCoins(Coin coin, int number);

  Map<Coin, Integer> getCoinsMap();

  void setCoinsMap(Map<Coin, Integer> availableCoins);
}
