package tdd.vendingMachine.state;

import tdd.vendingMachine.change.ChangeCalculator;
import tdd.vendingMachine.change.ILPChangeCalculator;
import tdd.vendingMachine.change.OperationResultWrapper;
import tdd.vendingMachine.change.SimpleChangeCalculator;
import tdd.vendingMachine.entities.Coin;
import tdd.vendingMachine.entities.Shelf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents state of the vending machine. Provides methods to insert/calculate change and select
 * required product.
 */
public class DefaultVendingMachineState implements VendingMachineState {

  private List<Shelf> shelves;
  private Shelf selectedShelf;
  private BigDecimal insertedAmount;
  private CoinWallet insertedCoinsWallet;
  private ChangeCalculator simpleChangeCalculator;
  private ChangeCalculator ilpChangeCalculator;
  private CoinWallet vendingMachineCoinWallet;

  public DefaultVendingMachineState() {
    this.shelves = new ArrayList<>();
    this.vendingMachineCoinWallet = new DefaultCoinWallet(true);
    this.insertedCoinsWallet = new DefaultCoinWallet(false);
    this.insertedAmount = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
    this.simpleChangeCalculator = new SimpleChangeCalculator();
    this.ilpChangeCalculator = new ILPChangeCalculator();
  }

  @Override
  public OperationResultWrapper insertChange(BigDecimal amount) {
    if (selectedShelf == null) {
      return null;
    }
    insertedAmount = insertedAmount.add(amount);
    insertedCoinsWallet.addCoins(new Coin(amount), 1);
    if (insertedAmount.compareTo(selectedShelf.getProduct().getPrice()) >= 0) {
      return calculateChangeAndReleaseProduct(insertedAmount.subtract(
          selectedShelf.getProduct().getPrice()));
    }
    return null;
  }

  @Override
  public OperationResultWrapper calculateChangeAndReleaseProduct(BigDecimal changeRequired) {
    if (selectedShelf == null) {
      return null;
    }

    Map<Coin, Integer> change = simpleChangeCalculator.calculateChange(changeRequired,
        vendingMachineCoinWallet);
    if (change == null) {
      change = ilpChangeCalculator.calculateChange(changeRequired, vendingMachineCoinWallet);
    }

    if (change != null) {
      selectedShelf.removeSingleProduct();
      selectedShelf = null;

      // add coins inserted by the client
      for (Map.Entry<Coin, Integer> entry : insertedCoinsWallet.getCoinsMap().entrySet()) {
        vendingMachineCoinWallet.addCoins(entry.getKey(), entry.getValue());
      }

      // remove coins used for change
      for (Map.Entry<Coin, Integer> entry : change.entrySet()) {
        vendingMachineCoinWallet.removeCoins(entry.getKey(), entry.getValue());
      }

      insertedCoinsWallet.setCoinsMap(new HashMap<>());
      insertedAmount = new BigDecimal(0);

      return new OperationResultWrapper(true, change);
    }
    insertedCoinsWallet.setCoinsMap(new HashMap<>());
    insertedAmount = new BigDecimal(0);
    return new OperationResultWrapper(false, null);
  }

  @Override
  public void setSelectedShelf(Shelf selectedShelf) {
    if (selectedShelf.getQuantity() <= 0) {
      return;
    }
    this.selectedShelf = selectedShelf;
  }

  @Override
  public void addShelf(Shelf shelf) {
    this.shelves.add(shelf);
  }

  @Override
  public List<Shelf> getShelves() {
    return shelves;
  }

  @Override
  public Shelf getSelectedShelf() {
    return selectedShelf;
  }

  @Override
  public BigDecimal getRemainingAmount() {
    return selectedShelf.getProduct().getPrice().subtract(insertedAmount);
  }

  @Override
  public void cancelOperation() {
    selectedShelf = null;
    insertedCoinsWallet.setCoinsMap(new HashMap<>());
  }

  public BigDecimal getInsertedAmount() {
    return insertedAmount;
  }

  public CoinWallet getInsertedCoinsWallet() {
    return insertedCoinsWallet;
  }

  void setSimpleChangeCalculator(ChangeCalculator simpleChangeCalculator) {
    this.simpleChangeCalculator = simpleChangeCalculator;
  }

  void setIlpChangeCalculator(ChangeCalculator ilpChangeCalculator) {
    this.ilpChangeCalculator = ilpChangeCalculator;
  }

  void setInsertedCoinsWallet(CoinWallet insertedCoinsWallet) {
    this.insertedCoinsWallet = insertedCoinsWallet;
  }

  void setVendingMachineCoinWallet(CoinWallet vendingMachineCoinWallet) {
    this.vendingMachineCoinWallet = vendingMachineCoinWallet;
  }
}
