package tdd.vendingMachine;

public class Runner {

  public static void main(String[] args) {
    VendingMachine vendingMachine = new VendingMachine();
    vendingMachine.start();
  }
}
