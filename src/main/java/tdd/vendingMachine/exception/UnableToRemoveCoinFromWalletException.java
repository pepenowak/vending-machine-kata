package tdd.vendingMachine.exception;

/**
 * Exception thrown when unable to remove coin from wallet.
 * It is used as an example of subclassing RuntimeException.
 */
public class UnableToRemoveCoinFromWalletException extends RuntimeException {

  public UnableToRemoveCoinFromWalletException(String message) {
    super(message);
  }
}
