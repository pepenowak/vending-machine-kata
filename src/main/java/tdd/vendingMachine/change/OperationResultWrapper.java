package tdd.vendingMachine.change;

import tdd.vendingMachine.entities.Coin;

import java.util.Map;

public class OperationResultWrapper {

  private final boolean ableToGiveChange;
  private final Map<Coin, Integer> change;

  public OperationResultWrapper(boolean cannotGiveChange, Map<Coin, Integer> change) {
    this.ableToGiveChange = cannotGiveChange;
    this.change = change;
  }

  public boolean isAbleToGiveChange() {
    return ableToGiveChange;
  }

  public Map<Coin, Integer> getChange() {
    return change;
  }
}
