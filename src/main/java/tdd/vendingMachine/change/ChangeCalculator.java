package tdd.vendingMachine.change;

import tdd.vendingMachine.state.CoinWallet;
import tdd.vendingMachine.entities.Coin;

import java.math.BigDecimal;
import java.util.Map;

public interface ChangeCalculator {
  Map<Coin, Integer> calculateChange(BigDecimal amount, CoinWallet coinWallet);
}
