package tdd.vendingMachine.change;

import tdd.vendingMachine.state.CoinWallet;
import tdd.vendingMachine.entities.Coin;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * SimpleChangeCalculator uses one of the simplest possible ways to calculate how to give required
 * change. It checks in order how many coins of the highest value should be given back and then
 * continues with coins of the lower value. Please see example below:
 *
 * Coins available: 1 x 5.00, 0 x 2.00, 4 x 1.00, 1 x 0.20, 7 x 0.10 Required change: 7.40 Result: 1
 * x 5.00 + 2 x 1.00 + 1 x 0.20 + 2 x 0.10
 */
public class SimpleChangeCalculator implements ChangeCalculator {

  public Map<Coin, Integer> calculateChange(BigDecimal amount, CoinWallet coinWallet) {

    Map<Coin, Integer> map = new HashMap<>();
    List<Coin> sortedCoins = coinWallet.getCoinsMap().keySet().stream().
        sorted(Comparator.comparing(Coin::getValue).reversed()).collect(Collectors.toList());

    for (Coin c : sortedCoins) {
      BigDecimal[] divisionResult = amount.divideAndRemainder(c.getValue());
      int quotient = divisionResult[0].intValue();
      if (quotient <= coinWallet.getCoinsMap().get(c)) {
        map.put(c, divisionResult[0].intValue());
        amount = divisionResult[1].setScale(2, BigDecimal.ROUND_HALF_UP);
      } else {
        int maxCoins = coinWallet.getCoinsMap().get(c);
        map.put(c, maxCoins);
        amount = amount.subtract(c.getValue().multiply(new BigDecimal(maxCoins)))
            .setScale(2, BigDecimal.ROUND_HALF_UP);
      }
    }

    if (amount.compareTo(new BigDecimal(0)) == 0) {
      return map;
    }
    return null;
  }

}
