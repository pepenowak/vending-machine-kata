package tdd.vendingMachine.change;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import scpsolver.problems.LPSolution;
import scpsolver.problems.LPWizard;
import scpsolver.problems.LPWizardConstraint;
import tdd.vendingMachine.state.CoinWallet;
import tdd.vendingMachine.entities.Coin;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * ILPChangeCalculator is used to calculate the way to give the required amount of change using
 * coins provided. It solves the problem of SimpleChangeCalculator and gives change taking into
 * account all available coins and not selecting always highest value coins first. Change
 * calculation is done via modelling the problem as Integer Linear Programming problem. Please see
 * sample below:
 *
 * Required change: 8.00 Available coins: 1 x 5.00 and 4 x 2.00 (NOTE: SimpleChangeCalculator would
 * first take 1 x 5.00 and have 3.00 left, then it would take 1 x 2.00 and have 1.00 left.
 * Therefore, it would not be able to give change as there is no 1.00 coin available.)
 *
 * Problem modelled as:
 *
 * Maximize z = 5x + 2y (where 5 and 2 are coin values and x and y are numbers of coins required)
 * Subject to: 5x+2y <= 8 , (where 8 is the amount of change required) 0 <= x <= 1 0 <= y <= 4
 *
 * Therefore the maximum possible value is always amount of change (in this case 8.00). If the
 * solution to objective function has the exact amount of change required (8.00), we know that the
 * vending machine is able to give away correct change using coins available. In this case the
 * solution has variables: z=8 when x=0 and y=4 (0 x 5.00 + 4 x 2.00 = 8.00)
 */
public class ILPChangeCalculator implements ChangeCalculator {

  private static final Logger LOGGER = LogManager.getLogger(ILPChangeCalculator.class);

  @Override
  public Map<Coin, Integer> calculateChange(BigDecimal amount, CoinWallet coinWallet) {
    if (coinWallet.getCoinsMap().keySet().size() <= 0) {
      return null;
    }
    LPWizard lpw = new LPWizard();
    LPWizardConstraint totalConstraint = lpw.addConstraint("c0", amount.doubleValue(), ">=");
    Map<String, Coin> variableToCoinMap = new HashMap<>();

    int varIndex = 1;
    int constraintIndex = 1;
    for (Coin c : coinWallet.getCoinsMap().keySet()) {
      String varName = "x" + varIndex;
      variableToCoinMap.put(varName, c);
      Double coinValue = c.getValue().doubleValue();
      Integer availableCoinsNumber = coinWallet.getCoinsMap().get(c);
      lpw.plus(varName, coinValue);
      totalConstraint.plus(varName, coinValue);
      lpw.addConstraint("c" + constraintIndex, availableCoinsNumber, ">=").plus(varName, 1);
      constraintIndex++;
      lpw.addConstraint("c" + constraintIndex, 0, "<=").plus(varName, 1);
      varIndex++;
      constraintIndex++;
    }
    lpw.setAllVariablesInteger();
    lpw.setMinProblem(false);


    LPSolution lpSolution = lpw.solve();
    if (lpSolution.getObjectiveValue() != amount.doubleValue()) {
      LOGGER.info("Objective function value is lower than required change. Therefore it is not " +
              "possible to give change for amount: {} and available coins: {}",
          amount, coinWallet.getCoinsMap());
      return null;
    }
    Map<Coin, Integer> changeResult = new HashMap<>();
    for (Map.Entry<String, Coin> entry : variableToCoinMap.entrySet()) {
      changeResult.put(entry.getValue(), Math.toIntExact(lpSolution.getInteger(entry.getKey())));
    }
    return changeResult;
  }
}
