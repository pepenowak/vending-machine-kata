vending-machine-kata
====================

Source control
--------------

This project was mainly developed on a computer where it was not possible to install external software (e.g. git).
Therefore it was not possible to commit subsequent changes. The only reason for it was lack of time and another laptop. 

Key aspects
----------------

When creating a solution main focus was given to the interesting part of calculating change. 
Due to lack of time the rest of the code was developed with a higher pace.

External dependencies
---------------------

When calculating the change, ILPChangeCalculator uses some ILP solvers. They are not available in general maven repo.
Therefore they were uploaded as 3rd party jars to a private nexus repo. That allows mvn build to work.

Build
-----

Run 'mvn package' to create a runnable jar with dependencies. Code can then be ran using: 
java -jar vending-machine-kata-1.0-jar-with-dependencies.jar